package AdamB.PollApp.controllers;

import AdamB.PollApp.models.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    UserService userService;

    @Autowired
    MainController(UserService userService){
        this.userService = userService;
    }

    @GetMapping("/")
    public String index(Model model){

        if(userService.getUserData()==null){
            return "redirect:login";
        }

        model.addAttribute("user", userService);
        model.addAttribute("info", userService.isLogin()?"You are logged in as "
                +userService.getUserData().getLogin():
                "You are not logged in");
        return "index";
        }

}
