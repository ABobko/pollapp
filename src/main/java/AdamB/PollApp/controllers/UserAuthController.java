package AdamB.PollApp.controllers;

import AdamB.PollApp.models.forms.LoginForm;
import AdamB.PollApp.models.forms.RegisterForm;
import AdamB.PollApp.models.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserAuthController {

    private UserService userService;

    @Autowired
    public UserAuthController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registerForm", new RegisterForm());
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute RegisterForm registerForm,
                           Model model) {
        if (userService.isUserExistByLogin(registerForm.getLogin())) {
            model.addAttribute("info", "login name already exists");
            return "register";
        }
        userService.registerUser(registerForm);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("loginForm",new LoginForm());
        return "login";
    }

    @PostMapping
    public String login(@ModelAttribute LoginForm loginForm,
                        Model model){
        if(!userService.loginUser(loginForm.getLogin(),loginForm.getPassword())){
            model.addAttribute("info", "Login failed: incorrect user name or password");
            return "login";
        }
        return "redirect:/";
    }

    @GetMapping("/logout")
    public String logout(){
        userService.logoutUser();
        return "redirect:/login";
    }



}
