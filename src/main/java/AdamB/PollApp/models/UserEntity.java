package AdamB.PollApp.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="users")
@Data
public class UserEntity {
    @Id
    @GeneratedValue
    private int id;
    private String login;
    @Column(name = "pass")
    private String password;
    private String email;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "votes_users")
    private List<VoteEntity> votes;

}
