package AdamB.PollApp.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="vote")
@Data
public class VoteEntity {
    @Id
    @GeneratedValue
    private int id;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "votes_users")
    private Set<UserEntity> users;

    @OneToMany(mappedBy = "vote")
    private List<AnswerEntity> answers;

}
