package AdamB.PollApp.models.repositories;

import AdamB.PollApp.models.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    boolean existsByLogin(String login);
    Optional<UserEntity> findByLoginAndPassword(String login, String password);
}
