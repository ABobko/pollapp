package AdamB.PollApp.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
public class AnswerEntity {
    @GeneratedValue
    private int id;
    private String content;
    @JoinColumn(name="vote_id")
    @ManyToOne
    private VoteEntity vote;


}
