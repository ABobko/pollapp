package AdamB.PollApp.models.services;

import AdamB.PollApp.models.UserEntity;
import AdamB.PollApp.models.forms.LoginForm;
import AdamB.PollApp.models.forms.RegisterForm;
import AdamB.PollApp.models.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS, value = "session")
public class UserService {

    private UserRepository userRepository;
    private UserEntity userData;
    private boolean isLogin;

    @Autowired
    UserService (UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public boolean isUserExistByLogin(String login){
        return userRepository.existsByLogin(login);
    }

    public boolean registerUser(RegisterForm registerForm){
        UserEntity newUser = new UserEntity();
        newUser.setLogin(registerForm.getLogin());
        newUser.setPassword(registerForm.getPassword());

        userRepository.save(newUser);
        return true;
    }

    public boolean loginUser(String login, String password){
        Optional<UserEntity> loggedUser =
                userRepository.findByLoginAndPassword(login,password);
        if(loggedUser.isPresent()){
            isLogin = true;
            userData = loggedUser.get();
        }
        return loggedUser.isPresent();
    }

    public void logoutUser(){
        isLogin = false;
        userData = null;
    }

    public boolean isLogin(){
        return isLogin;
    }


    public UserEntity getUserData(){
        return userData;
    }
}
